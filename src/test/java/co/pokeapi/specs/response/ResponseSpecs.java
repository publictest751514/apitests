package co.pokeapi.specs.response;

import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.ResponseSpecification;

import static co.pokeapi.config.Config.HTTP_CODE_OK;

public class ResponseSpecs {
    public static ResponseSpecification response200Spec = new ResponseSpecBuilder()
            .expectContentType(ContentType.JSON)
            .expectStatusCode(HTTP_CODE_OK)
            .build();
}
