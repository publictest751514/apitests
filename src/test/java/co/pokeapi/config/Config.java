package co.pokeapi.config;

public class Config {
    public static final String BASE_URI = "https://pokeapi.co/api/v2/pokemon/";

    public static final int HTTP_CODE_OK = 200;
}
