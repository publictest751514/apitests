package co.pokeapi.pojo.pokemon_list_limit;

import java.util.List;

import lombok.Getter;
import co.pokeapi.pojo.pokemon_list_limit.pokemon_list_limit.ResultsItem;

@Getter
public class PokemonListLimit {

    private String next;

    private Object previous;

    private int count;

    private List<ResultsItem> results;
}