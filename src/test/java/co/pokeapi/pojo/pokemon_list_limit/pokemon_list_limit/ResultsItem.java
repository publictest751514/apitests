package co.pokeapi.pojo.pokemon_list_limit.pokemon_list_limit;

import lombok.Getter;

@Getter
public class ResultsItem {

    private String name;

    private String url;
}