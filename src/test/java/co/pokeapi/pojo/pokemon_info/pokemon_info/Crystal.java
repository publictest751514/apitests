package co.pokeapi.pojo.pokemon_info.pokemon_info;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Crystal {

    @JsonProperty("back_transparent")
    private String backTransparent;

    @JsonProperty("back_shiny_transparent")
    private String backShinyTransparent;

    @JsonProperty("back_default")
    private String backDefault;

    @JsonProperty("front_default")
    private String frontDefault;

    @JsonProperty("front_transparent")
    private String frontTransparent;

    @JsonProperty("front_shiny_transparent")
    private String frontShinyTransparent;

    @JsonProperty("back_shiny")
    private String backShiny;

    @JsonProperty("front_shiny")
    private String frontShiny;
}