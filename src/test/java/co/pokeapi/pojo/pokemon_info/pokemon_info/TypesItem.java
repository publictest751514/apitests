package co.pokeapi.pojo.pokemon_info.pokemon_info;

import co.pokeapi.pojo.pokemon_info.pokemon_info.type.Type;
import lombok.Getter;

@Getter
public class TypesItem {

    private int slot;

    private Type type;
}