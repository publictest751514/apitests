package co.pokeapi.pojo.pokemon_info.pokemon_info;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Other {

    @JsonProperty("dream_world")
    private DreamWorld dreamWorld;

    @JsonProperty("official-artwork")
    private OfficialArtwork officialArtwork;

    private Home home;
}