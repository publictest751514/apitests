package co.pokeapi.pojo.pokemon_info.pokemon_info;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class VersionGroupDetailsItem {

    @JsonProperty("level_learned_at")
    private int levelLearnedAt;

    @JsonProperty("version_group")
    private VersionGroup versionGroup;

    @JsonProperty("move_learn_method")
    private MoveLearnMethod moveLearnMethod;
}