package co.pokeapi.pojo.pokemon_info.pokemon_info;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class AbilitiesItem {

    @JsonProperty("is_hidden")
    private boolean isHidden;

    private Ability ability;

    private int slot;
}