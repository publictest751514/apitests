package co.pokeapi.pojo.pokemon_info.pokemon_info;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class StatsItem {

    private Stat stat;

    @JsonProperty("base_stat")
    private int baseStat;

    private int effort;
}