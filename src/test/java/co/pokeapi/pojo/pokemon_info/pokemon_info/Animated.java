package co.pokeapi.pojo.pokemon_info.pokemon_info;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Animated {

    @JsonProperty("back_shiny_female")
    private Object backShinyFemale;

    @JsonProperty("back_female")
    private Object backFemale;

    @JsonProperty("back_default")
    private String backDefault;

    @JsonProperty("front_shiny_female")
    private Object frontShinyFemale;

    @JsonProperty("front_default")
    private String frontDefault;

    @JsonProperty("front_female")
    private Object frontFemale;

    @JsonProperty("back_shiny")
    private String backShiny;

    @JsonProperty("front_shiny")
    private String frontShiny;
}