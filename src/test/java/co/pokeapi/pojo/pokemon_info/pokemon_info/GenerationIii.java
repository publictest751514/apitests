package co.pokeapi.pojo.pokemon_info.pokemon_info;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class GenerationIii {

    @JsonProperty("firered-leafgreen")
    private FireredLeafgreen fireredLeafgreen;

    @JsonProperty("ruby-sapphire")
    private RubySapphire rubySapphire;

    private Emerald emerald;
}