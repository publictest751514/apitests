package co.pokeapi.pojo.pokemon_info.pokemon_info;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class GameIndicesItem {

    @JsonProperty("game_index")
    private int gameIndex;

    private Version version;
}