package co.pokeapi.pojo.pokemon_info.pokemon_info;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class XY {

    @JsonProperty("front_shiny_female")
    private Object frontShinyFemale;

    @JsonProperty("front_default")
    private String frontDefault;

    @JsonProperty("front_female")
    private Object frontFemale;

    @JsonProperty("front_shiny")
    private String frontShiny;
}