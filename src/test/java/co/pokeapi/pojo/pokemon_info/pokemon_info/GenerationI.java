package co.pokeapi.pojo.pokemon_info.pokemon_info;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class GenerationI {

    private Yellow yellow;

    @JsonProperty("red-blue")
    private RedBlue redBlue;
}