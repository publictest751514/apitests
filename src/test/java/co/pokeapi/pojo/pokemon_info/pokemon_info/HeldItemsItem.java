package co.pokeapi.pojo.pokemon_info.pokemon_info;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class HeldItemsItem {

    private Item item;

    @JsonProperty("version_details")
    private List<VersionDetailsItem> versionDetails;
}