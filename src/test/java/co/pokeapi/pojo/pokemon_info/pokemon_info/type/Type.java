package co.pokeapi.pojo.pokemon_info.pokemon_info.type;

import lombok.Getter;

@Getter
public class Type {

    private String name;

    private String url;
}