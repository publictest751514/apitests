package co.pokeapi.pojo.pokemon_info.pokemon_info;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class GenerationVi {

    @JsonProperty("omegaruby-alphasapphire")
    private OmegarubyAlphasapphire omegarubyAlphasapphire;

    @JsonProperty("x-y")
    private XY xY;
}