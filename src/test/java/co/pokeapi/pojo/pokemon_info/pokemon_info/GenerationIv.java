package co.pokeapi.pojo.pokemon_info.pokemon_info;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class GenerationIv {

    private Platinum platinum;

    @JsonProperty("diamond-pearl")
    private DiamondPearl diamondPearl;

    @JsonProperty("heartgold-soulsilver")
    private HeartgoldSoulsilver heartgoldSoulsilver;
}