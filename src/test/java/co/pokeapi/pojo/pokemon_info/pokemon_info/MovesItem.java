package co.pokeapi.pojo.pokemon_info.pokemon_info;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class MovesItem {

    @JsonProperty("version_group_details")
    private List<VersionGroupDetailsItem> versionGroupDetails;

    private Move move;
}