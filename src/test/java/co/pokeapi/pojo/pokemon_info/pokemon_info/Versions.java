package co.pokeapi.pojo.pokemon_info.pokemon_info;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Versions {

    @JsonProperty("generation-iii")
    private GenerationIii generationIii;

    @JsonProperty("generation-ii")
    private GenerationIi generationIi;

    @JsonProperty("generation-v")
    private GenerationV generationV;

    @JsonProperty("generation-iv")
    private GenerationIv generationIv;

    @JsonProperty("generation-vii")
    private GenerationVii generationVii;

    @JsonProperty("generation-i")
    private GenerationI generationI;

    @JsonProperty("generation-viii")
    private GenerationViii generationViii;

    @JsonProperty("generation-vi")
    private GenerationVi generationVi;
}