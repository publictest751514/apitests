package co.pokeapi.pojo.pokemon_info.pokemon_info;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Yellow {

    @JsonProperty("front_gray")
    private String frontGray;

    @JsonProperty("back_transparent")
    private String backTransparent;

    @JsonProperty("back_default")
    private String backDefault;

    @JsonProperty("back_gray")
    private String backGray;

    @JsonProperty("front_default")
    private String frontDefault;

    @JsonProperty("front_transparent")
    private String frontTransparent;
}