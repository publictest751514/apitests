package co.pokeapi.pojo.pokemon_info.pokemon_info;

import lombok.Getter;

@Getter
public class VersionDetailsItem {

    private Version version;

    private int rarity;
}