package co.pokeapi.pojo.pokemon_info.pokemon_info;

import lombok.Getter;

@Getter
public class Item {

    private String name;

    private String url;
}