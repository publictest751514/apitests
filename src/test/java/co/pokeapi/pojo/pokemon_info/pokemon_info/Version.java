package co.pokeapi.pojo.pokemon_info.pokemon_info;

import lombok.Getter;

@Getter
public class Version {

    private String name;

    private String url;
}