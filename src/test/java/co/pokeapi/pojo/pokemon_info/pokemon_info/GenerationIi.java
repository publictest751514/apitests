package co.pokeapi.pojo.pokemon_info.pokemon_info;

import lombok.Getter;

@Getter
public class GenerationIi {

    private Gold gold;

    private Crystal crystal;

    private Silver silver;
}