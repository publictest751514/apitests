package co.pokeapi.pojo.pokemon_info;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import co.pokeapi.pojo.pokemon_info.pokemon_info.TypesItem;
import co.pokeapi.pojo.pokemon_info.pokemon_info.HeldItemsItem;
import co.pokeapi.pojo.pokemon_info.pokemon_info.Sprites;
import co.pokeapi.pojo.pokemon_info.pokemon_info.AbilitiesItem;
import co.pokeapi.pojo.pokemon_info.pokemon_info.GameIndicesItem;
import co.pokeapi.pojo.pokemon_info.pokemon_info.Species;
import co.pokeapi.pojo.pokemon_info.pokemon_info.StatsItem;
import co.pokeapi.pojo.pokemon_info.pokemon_info.MovesItem;
import co.pokeapi.pojo.pokemon_info.pokemon_info.FormsItem;
import lombok.Getter;

@Getter
public class PokemonInfo {

    @JsonProperty("location_area_encounters")
    private String locationAreaEncounters;

    private List<TypesItem> types;

    @JsonProperty("base_experience")
    private int baseExperience;

    @JsonProperty("held_items")
    private List<HeldItemsItem> heldItems;

    private int weight;

    @JsonProperty("is_default")
    private boolean isDefault;

    @JsonProperty("past_types")
    private List<Object> pastTypes;

    private Sprites sprites;

    private List<AbilitiesItem> abilities;

    @JsonProperty("game_indices")
    private List<GameIndicesItem> gameIndices;

    private Species species;

    private List<StatsItem> stats;

    private List<MovesItem> moves;

    private String name;

    private int id;

    private List<FormsItem> forms;

    private int height;

    private int order;
}