package co.pokeapi.helpers;

import co.pokeapi.helpers.base.BaseHelper;
import co.pokeapi.pojo.pokemon_list_limit.PokemonListLimit;
import co.pokeapi.pojo.pokemon_info.PokemonInfo;
import io.qameta.allure.Step;

import static co.pokeapi.specs.request.RequestSpecs.requestSpec;
import static co.pokeapi.specs.response.ResponseSpecs.response200Spec;
import static io.restassured.RestAssured.given;

public class PokemonInfoHelper extends BaseHelper {

    @Step("Получить полную информацию о покемоне \"{name}\"")
    public static PokemonInfo getPokemonInfoByName(String name) {
        return given()
                .spec(requestSpec)
            .when()
                .get(name)
            .then()
                .spec(response200Spec)
                .extract()
                .as(PokemonInfo.class);
    }

    @Step("Получить список покемонов с лимитом: \"{limit}\"")
    public static PokemonListLimit getLimitedPokemonsList(int limit) {
        return given()
                .spec(requestSpec)
                .queryParam("limit", limit)
            .when()
                .get()
            .then()
                .spec(response200Spec)
                .extract()
                .as(PokemonListLimit.class);
    }
}
