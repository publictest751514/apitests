package co.pokeapi.helpers.base;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;

public class BaseHelper {
    static {
        RestAssured.filters(new AllureRestAssured());
    }
}
