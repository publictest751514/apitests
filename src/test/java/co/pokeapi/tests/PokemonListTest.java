package co.pokeapi.tests;

import co.pokeapi.helpers.PokemonInfoHelper;
import co.pokeapi.pojo.pokemon_list_limit.pokemon_list_limit.ResultsItem;
import com.github.javafaker.Faker;
import co.pokeapi.pojo.pokemon_list_limit.PokemonListLimit;
import io.qameta.allure.Description;

import org.assertj.core.api.SoftAssertions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class PokemonListTest {

    SoftAssertions soft = new SoftAssertions();

    @Test(description = "Получение лимитированного списка", dataProvider = "limitDataProvider")
    @Description("Проверка получения лимитированного списка покемонов")
    public void getLimitList(int limit) {
        PokemonListLimit pokemonsList = PokemonInfoHelper.getLimitedPokemonsList(limit);

        soft.assertThat(limit)
                .as("Сравнение размера списка")
                .isEqualTo(pokemonsList.getResults().size());

        List<String> namesList = pokemonsList.getResults()
                .stream()
                        .map(ResultsItem::getName)
                                .toList();

        soft.assertThat(namesList)
                .as("Список не должен содержать значение null или быть пустым")
                .allSatisfy(s -> assertThat(s)
                .isNotNull()
                .isNotEmpty());

        soft.assertAll();
    }

    @DataProvider(name = "limitDataProvider")
    private Object[][] limitData() {
        Faker faker = new Faker();
        return new Object[][] {
            {faker.number().numberBetween(2, 10)},
            {faker.number().numberBetween(11, 20)}
        };
    }
}
