package co.pokeapi.tests;

import co.pokeapi.pojo.pokemon_info.PokemonInfo;
import co.pokeapi.helpers.PokemonInfoHelper;
import io.qameta.allure.Description;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


public class ComparePokemonsTest {

    private final String FIRST_POKEMON_NANE = "rattata";

    private final String SECOND_POKEMON_NAME = "pidgeotto";

    private final String ABILITY = "run-away";

    private PokemonInfo rattataPokemon;

    private PokemonInfo pidgeottoPokemon;

    @BeforeClass(description = "Получить данные покемонов")
    public void getPokemons() {
        this.rattataPokemon = PokemonInfoHelper.getPokemonInfoByName(FIRST_POKEMON_NANE);
        this.pidgeottoPokemon = PokemonInfoHelper.getPokemonInfoByName(SECOND_POKEMON_NAME);
    }

    @Test(description = "Сравнение веса покемонов")
    @Description("Сравнение значений веса двух покемонов")
    public void comparePokemonsWeight() {
        int rattataWeight = this.rattataPokemon.getWeight();
        int pidgeottoWeight = this.pidgeottoPokemon.getWeight();

        assertThat(rattataWeight)
                .as("Вес первого покемона ниже веса первого покемона")
                .isLessThan(pidgeottoWeight);
    }

    @Test(description = "Проверка наличия у покемона умения", dataProvider = "pokemonsDataProvider")
    @Description("Проверка наличия у покемона умения")
    public void checkPokemonHasAbility(List<String> abilitiesList, boolean isTrue) {
        boolean canPokemonRunAway = abilitiesList.contains(ABILITY);

        assertThat(isTrue)
                .as("Наличие умения у покемона")
                .isEqualTo(canPokemonRunAway);
    }

    @DataProvider(name = "pokemonsDataProvider")
    private Object[][] data() {

        List<String> rattataAbilitiesList = this.rattataPokemon.getAbilities()
                .stream()
                .map(ability -> ability.getAbility().getName())
                .toList();

        List<String> pidgeottoAbilitiesList = this.pidgeottoPokemon.getAbilities()
                .stream()
                .map(ability -> ability.getAbility().getName())
                .toList();

        return new Object[][] {
            {rattataAbilitiesList, true}, {pidgeottoAbilitiesList, false}
        };
    }
}
