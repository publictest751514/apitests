
# API Autotests

Проект API-автотестов сервиса https://pokeapi.co/


## Стэк

- Java
- Rest Assured
- TestNG
- Maven
- Allure


## Документация

[Список тест-кейсов](https://docs.google.com/document/d/1ZboH1Azl-AUPJj1HlkXdAh5I_TAzlJSL0Fe5XksUyDA/edit?usp=sharing)


## Структура проекта

- [pojo] - POJO классы

- [tests] - тесты

- [config] - конфигурационные файлы

- [pom.xml] - конфигурация сборщика Maven

- [.gitlab-ci.yml] - конфигурация CI Gitlab

- [testng.xml] - конфигурация TestNG


## Запуск

Запуск тестов

```bash
  mvn clean test
```

Формирование и отображение allure отчета в отдельном окне браузера

```bash
  mvn allure:report
  mvn allure:serve
```


